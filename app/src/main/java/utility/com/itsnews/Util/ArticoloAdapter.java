package utility.com.itsnews.Util;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import utility.com.itsnews.Activity.LoadingActivity;
import utility.com.itsnews.Entity.Articolo;
import utility.com.itsnews.R;

/**
 * Created by utente on 21/04/2017.
 */

public class ArticoloAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Articolo> lista;
    private ImageLoader mImageLoader;

    public ArticoloAdapter(Context context, ArrayList<Articolo> lista) {
        this.context = context;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).getId();
    }


    class ViewHolder {
        private TextView titolo;
        private TextView testoAbb;
        private NetworkImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        if(position == 0)
            view = inflater.inflate(R.layout.cell_primo_articolo, null);
        else
            view = inflater.inflate(R.layout.cell_articolo, null);
        ViewHolder holder = new ViewHolder();
        holder.titolo = (TextView) view.findViewById(R.id.txtTitolo);
        holder.testoAbb = (TextView) view.findViewById(R.id.txtTestoAbb);
        holder.img = (NetworkImageView) view.findViewById(R.id.imgArticolo);


        Collections.sort(lista, new Comparator<Articolo>() {
            public int compare(Articolo synchronizedListOne, Articolo synchronizedListTwo) {
            //use instanceof to verify the references are indeed of the type in question
                return (synchronizedListOne.getData().compareTo(synchronizedListTwo.getData()));
            }
        });

        lista = reverse(lista);

//        ValueAnimator animation = ValueAnimator.ofFloat(200f, 0f);
//        animation.setDuration(1000);
//        animation.start();
//
//        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator updatedAnimation) {
//                float animatedValue = (float)updatedAnimation.getAnimatedValue();
//                if(position %2 == 0) {
//                    view.setTranslationX(animatedValue);
//                }
//                else {
//                    view.setTranslationY(animatedValue);
//                }
//            }
//        });

        if(position %2 == 0) {
            view.setBackgroundResource(R.color.notizia1);
            holder.img.setBackgroundResource(R.color.notizia1);
        }
        else {
            view.setBackgroundResource(R.color.notizia2);
            holder.img.setBackgroundResource(R.color.notizia2);
        }

        Articolo a = (Articolo) getItem(position);
        holder.titolo.setText(a.getTitolo());
        holder.testoAbb.setText(a.getTestoAbb());


        mImageLoader = CustomVolleyRequestQueue.getInstance(context).getImageLoader();
        //Image URL - This can point to any image file supported by Android
        final String url = a.getImmagine();
        mImageLoader.get(url, ImageLoader.getImageListener(holder.img,
                R.mipmap.ic_launcher, android.R.drawable.ic_dialog_alert));
        holder.img.setImageUrl(url, mImageLoader);

        return view;
    }

    public ArrayList<Articolo> reverse(ArrayList<Articolo> list) {
        if(list.size() > 1) {
            Articolo value = list.remove(0);
            reverse(list);
            list.add(value);
        }
        return list;
    }
}
