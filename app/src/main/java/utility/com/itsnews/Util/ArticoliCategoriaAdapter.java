package utility.com.itsnews.Util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import utility.com.itsnews.Entity.Articolo;
import utility.com.itsnews.R;

/**
 * Created by utente on 21/04/2017.
 */

public class ArticoliCategoriaAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Articolo> lista;
    private ImageLoader mImageLoader;

    public ArticoliCategoriaAdapter(Context context, ArrayList<Articolo> lista) {
        this.context = context;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).getId();
    }

    class ViewHolder {
        private TextView titolo;
        private TextView testo;
        private TextView data;
        private NetworkImageView img;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.cell_articoli, null);
            ViewHolder holder = new ViewHolder();
            holder.titolo = (TextView) convertView.findViewById(R.id.txtTitolo);
            holder.testo = (TextView) convertView.findViewById(R.id.txtTesto);
            holder.img = (NetworkImageView) convertView.findViewById(R.id.img);
            holder.data = (TextView) convertView.findViewById(R.id.txtData);
            convertView.setTag(holder);
        }

        ViewHolder holder2 = (ViewHolder) convertView.getTag();
        Articolo a = (Articolo) getItem(position);
        holder2.titolo.setText(a.getTitolo());
        holder2.testo.setText(a.getTesto());
        holder2.data.setText(a.getData());

        mImageLoader = CustomVolleyRequestQueue.getInstance(context).getImageLoader();

        final String url = a.getImmagine();
        mImageLoader.get(url, ImageLoader.getImageListener(holder2.img,
                R.mipmap.ic_launcher, android.R.drawable.ic_dialog_alert));
        holder2.img.setImageUrl(url, mImageLoader);
        return convertView;
    }
}
