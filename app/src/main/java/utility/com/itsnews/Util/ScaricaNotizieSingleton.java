package utility.com.itsnews.Util;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import utility.com.itsnews.Entity.Articolo;

/**
 * Created by utente on 04/05/2017.
 */

public class ScaricaNotizieSingleton {

    private Context context;
    public static ArrayList<Articolo> lista = new ArrayList<>();
    private static ScaricaNotizieSingleton instance = null;
    int number_of_pages = 0;

    public ScaricaNotizieSingleton() {

    }

    public static ScaricaNotizieSingleton getInstance() {

        if (instance == null) {
            instance = new ScaricaNotizieSingleton();
        }
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    public void getPages(final Context context, int index) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://isfrat.it/wp-json/wp/v2/pages";


        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONArray array = new JSONArray(response);

                        for(int i = 0;i < array.length(); i++) {

                            JSONObject obj = array.getJSONObject(i);
                            JSONObject obj1 = new JSONObject(obj.getString("guid"));
                            String s = obj1.getString("rendered");
                            number_of_pages = Integer.parseInt(s.substring(s.length() - 1, s.length()));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("d", "Errore Connessione");
                }
            }
        );

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }


    public void getNotizie(Context context, int id_categoria, int page, String search) {

        if(page == 1) {
            getPages(context, page);
        }

        this.context = context;
        if(page == 1)
            lista.clear();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url;
        if (search.equals("")) {
            if (id_categoria > 0) {
                url = "http://isfrat.it/wp-json/wp/v2/posts?page=" + page + "&categories=" + id_categoria;
            } else {
                url = "http://isfrat.it/wp-json/wp/v2/posts?page=" + page;
            }
        } else {
            url = "http://isfrat.it/wp-json/wp/v2/posts?search=" + search;
        }

        // Request a string response from the provided URL.
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (!response.equals("[]")) {
                                JSONArray array = new JSONArray(response);
                                JSONObject obj;
                                JSONObject obj2;
                                for (int i = 0; i < array.length(); i++) {
                                    obj = array.getJSONObject(i);
                                    Articolo art = new Articolo();
                                    obj2 = new JSONObject(obj.getString("title"));
                                    art.setTitolo(String.valueOf(stripHtml(obj2.getString("rendered"))));
                                    obj2 = new JSONObject(obj.getString("content"));
                                    art.setTesto(String.valueOf(stripHtml(obj2.getString("rendered"))));
                                    obj2 = new JSONObject(obj.getString("excerpt"));
                                    if(String.valueOf(stripHtml(obj2.getString("rendered"))).length() > 90)
                                        art.setTestoAbb(String.valueOf(stripHtml(obj2.getString("rendered"))).substring(0, 90) + "...");
                                    else
                                        art.setTestoAbb(String.valueOf(stripHtml(obj2.getString("rendered"))) + "...");

                                    art.setId(obj.getInt("id"));
                                    art.setData(obj.getString("date").substring(0, 10));
                                    String comodo = obj.getString("categories").replace("]", "").replace("[", "");
                                    String[] arraycomodo = comodo.split(",");
                                    int[] arrayint = new int[arraycomodo.length];
                                    for (int j = 0; j < arraycomodo.length; j++) {
                                        arrayint[j] = Integer.parseInt(arraycomodo[j]);
                                    }
                                    art.setCategoria(arrayint);

                                    art.setUrl(obj.getString("link"));

                                    caricaImmagine(obj.getString("featured_media"), art);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("d", "Errore Connessione");
                    }
                }
        );

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }

    public void caricaImmagine(String id, final Articolo art) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://isfrat.it/wp-json/wp/v2/media/" + id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONObject obj2 = new JSONObject(obj.getString("guid"));
                            art.setImmagine(String.valueOf(stripHtml(obj2.getString("rendered"))));

                            if (art.getImmagine() == null)
                                art.setImmagine("");

                            lista.add(art);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("d", "Errore caricamento immagine");
                    }
                }
        );
        queue.add(stringRequest);
    }

    public Spanned stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }
}
