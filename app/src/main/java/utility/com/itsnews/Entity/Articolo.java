package utility.com.itsnews.Entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by utente on 21/04/2017.
 */

public class Articolo {
    private int id;
    protected String titolo;
    protected String testo;
    protected String testoAbb;
    protected String immagine;
    protected String data;
    protected int[] categoria;
    protected String url;

    public Articolo() {
    }

    public Articolo(int id, String titolo, String testo, String testoAbb, String immagine, String data, int[] categoria, String url) {
        this.id = id;
        this.titolo = titolo;
        this.testo = testo;
        this.testoAbb = testoAbb;
        this.immagine = immagine;
        this.data = data;
        this.categoria = categoria;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    public String getTestoAbb() {
        return testoAbb;
    }

    public void setTestoAbb(String testoAbb) {
        this.testoAbb = testoAbb;
    }

    public String getImmagine() {
        return immagine;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int[] getCategoria() {
        return categoria;
    }

    public void setCategoria(int[] categoria) {
        this.categoria = categoria;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
