package utility.com.itsnews.Entity;

/**
 * Created by Utente on 04/05/2017.
 */

public class Utente {


    private String user;
    private String email;
    private boolean esiste;

    public Utente(String user, String email, boolean esiste) {
        this.user = user;
        this.email = email;
        this.esiste = esiste;
    }

    public Utente(){};

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEsiste() {
        return esiste;
    }

    public void setEsiste(boolean esiste) {
        this.esiste = esiste;
    }
}
