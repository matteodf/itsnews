package utility.com.itsnews.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import utility.com.itsnews.Entity.Articolo;
import utility.com.itsnews.Entity.Utente;
import utility.com.itsnews.R;
import utility.com.itsnews.Util.ScaricaNotizieSingleton;

public class LoginActivity extends AppCompatActivity {

    private Button btnAccedi;
    private Button btnGooglepiu;
    private TextView txtRegistrati;
    private EditText txtUsername;
    private EditText txtPassword;
    private String user;
    private String pass;
    Utente utente;
    Bundle b;
    Intent i;
    private LinearLayout linearLoading;
    private LinearLayout linearLogin;
    private SharedPreferences vPreferences;//preferences to save locally
    private SharedPreferences.Editor editor; //editor
    private View window;
    private CallbackManager callbackManager;
    private LoginButton btnFacebook;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        btnAccedi = (Button) findViewById(R.id.btnAccedi);
        btnGooglepiu = (Button) findViewById(R.id.btnGooglepiu);
        txtUsername = (EditText) findViewById(R.id.edtUser);
        txtPassword = (EditText) findViewById(R.id.edtPassword);
        txtRegistrati = (TextView) findViewById(R.id.txtRegistrati);
        window = findViewById(R.id.activity_log);
        linearLogin = (LinearLayout) findViewById(R.id.login);
        linearLoading = (LinearLayout) findViewById(R.id.loading);

        linearLoading.setVisibility(View.INVISIBLE);
        btnFacebook = (LoginButton) findViewById(R.id.btnFacebook);

        btnAccedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                user = txtUsername.getText().toString();
                pass = txtPassword.getText().toString();

                i = new Intent(LoginActivity.this, NotizieActivity.class);


                if (user.equals("") || pass.equals("")) {
                    Toast.makeText(LoginActivity.this, "Uno o più campi non sono stati riempiti, Controlla", Toast.LENGTH_SHORT).show();
                } else {
                    linearLoading.setVisibility(View.VISIBLE);
                    linearLogin.setVisibility(View.INVISIBLE);
                    Login(user, pass);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Log.d("risultato utente", "" + utente.isEsiste());
                            if (utente.isEsiste()) {

                                View view = getCurrentFocus();
                                if (view != null) {
                                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                }

                                startActivity(i);
                                finish();
                            } else {
                                linearLoading.setVisibility(View.INVISIBLE);
                                linearLogin.setVisibility(View.VISIBLE);
                                Toast.makeText(LoginActivity.this, "L'utente non esiste", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 4000);
                }


            }
        });

        String htmlString = "<u>Non sei registato? Registrati qui!</u>";
        txtRegistrati.setText(Html.fromHtml(htmlString));

        txtRegistrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegistratiActivity.class));
            }
        });


        btnFacebook.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday"));

        callbackManager = CallbackManager.Factory.create();

            // Callback registration
        btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.v("LoginActivity", loginResult.toString());
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.v("LoginActivity", response.toString());

                            // Application code
                            try {
                                //String email = object.getString("email");
                                //String birthday = object.getString("birthday"); // 01/31/1980 format

                                vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
                                editor = vPreferences.edit();
                                String[] data = object.getString("birthday").toString().split("/");
                                String compleanno = data[1]+"-"+data[0]+"-"+data[2];
                                Log.d("Compleanno", compleanno);
                                editor.putString("id_user", object.getString("id"));
                                editor.putString("username", object.getString("name"));
                                editor.putString("name", object.getString("name"));
                                editor.putString("email", object.getString("email"));
                                editor.putString("DataNascita", compleanno);
                                editor.putBoolean("logged", true);
                                editor.commit();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();

                startActivity(new Intent(LoginActivity.this, NotizieActivity.class));
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.v("LoginActivity", exception.getCause().toString());
            }
        });

        btnGooglepiu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    } // end: onCreate


    public void Login(final String User, final String PassUser) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://isfrat.it/wp-content/Login.php";
        final Utente ut = new Utente();
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String r = response.substring(0, response.indexOf("{"));

                    String r2 = response.substring(response.indexOf("{"), response.length());

                    try {
                        JSONObject obj = new JSONObject(r2);

                        if (r.equals("True")) {

                            ut.setUser(obj.getString("Username"));
                            ut.setEmail(obj.getString("Email"));
                            ut.setEsiste(true);


                            vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
                            editor = vPreferences.edit();

                            editor.putInt("id_user", obj.getInt("Id"));
                            editor.putString("username", obj.getString("Username"));
                            editor.putString("email", obj.getString("Email"));
                            editor.putString("DataNascita", obj.getString("DataDiNascita"));
                            editor.putString("nome", obj.getString("Nome") + "\n" +obj.getString("Cognome"));
                            editor.putString("imgProfilo",obj.getString("img_Profilo"));
                            editor.putString("password",PassUser);
                            editor.putBoolean("logged", true);
                            editor.commit();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("d", "Errore Connessione");
                }
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", User);
                params.put("password", PassUser);
                params.put("remember", "true");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
        utente = ut;
    }
}
