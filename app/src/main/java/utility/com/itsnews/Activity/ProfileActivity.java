package utility.com.itsnews.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import utility.com.itsnews.R;

public class ProfileActivity extends AppCompatActivity {
    TextView txtComple, txtNomeCogn, txtEmailProf;
    Button btnRitorna;
    String pass, confronta_pass, tipo, oldPass;
    private SharedPreferences vPreferences;
    private SharedPreferences.Editor editor;
    private View window;
    ImageView profilo;
    AlertDialog.Builder alert;
    ImageView img1, img2, img3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        alert = new AlertDialog.Builder(ProfileActivity.this);

        profilo = (ImageView) findViewById(R.id.profile_image);
        txtComple = (TextView) findViewById(R.id.txtNascita);
        txtEmailProf = (TextView) findViewById(R.id.txtEmailProfilo);
        txtNomeCogn = (TextView) findViewById(R.id.txtNomeProfilo);
        window = findViewById(R.id.activity_prof);
        vPreferences = window.getContext().getSharedPreferences("Preferences", 0);

        switch (vPreferences.getString("imgProfilo", "")) {
            case "deadpool":
                profilo.setImageResource(R.drawable.utente);
                break;
            case "android":
                profilo.setImageResource(android.R.drawable.sym_def_app_icon);
                break;
            case "camera":
                profilo.setImageResource(R.drawable.ic_menu_camera);
                break;
            default:
                profilo.setImageResource(R.drawable.com_facebook_profile_picture_blank_square);
                break;
        }

            if (vPreferences.getString("DataNascita", "Nessuna data").equals("Inserisci")) {
                String compl = txtComple.getText().toString() + " ";
                txtComple.setText(compl);
            }else{
                String compl = txtComple.getText().toString() + " " + vPreferences.getString("DataNascita", "Nessuna data");
                txtComple.setText(compl);
            }
            txtNomeCogn.setText(vPreferences.getString("nome", " "));
            txtEmailProf.setText(vPreferences.getString("email", ""));

            profilo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayoutInflater factory = LayoutInflater.from(ProfileActivity.this);

                    final View textEntryView = factory.inflate(R.layout.select_profile_image, null);
                    img1 = (ImageView) textEntryView.findViewById(R.id.imgProfi2);
                    img2 = (ImageView) textEntryView.findViewById(R.id.imgProfi3);
                    img3 = (ImageView) textEntryView.findViewById(R.id.imgProfi4);
                    alert.setView(textEntryView);
                    alert.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();

                                }
                            });
                    alert.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    alert.show();
                }
            });
        }

        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.modifica_profilo, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            int id = item.getItemId();
            switch (id) {
                case R.id.action_change: {
                    LayoutInflater factory = LayoutInflater.from(this);
                    final View textEntryView = factory.inflate(R.layout.alert_change_pass, null);
                    final EditText oldIn = (EditText) textEntryView.findViewById(R.id.edtOldPass);
                    final EditText input1 = (EditText) textEntryView.findViewById(R.id.edtNewPassword);
                    final EditText input2 = (EditText) textEntryView.findViewById(R.id.edtConfirm);
                    final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setView(textEntryView);
                    alert.setPositiveButton("CONFIRM",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    oldPass = oldIn.getText().toString();
                                    String supporto= vPreferences.getString("password", "");
                                    pass = input1.getText().toString();
                                    confronta_pass = input2.getText().toString();
                                    Log.d("Old Pass", vPreferences.getString("password", ""));
                                    Log.d("Old Pass DIALOG", oldPass);
                                    if (oldPass.equals(supporto)) {
                                        if (confronta_pass.equals(pass)) {
                                            changedCeck();
                                            vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
                                            editor = vPreferences.edit();
                                            editor.putString("password",pass);
                                            editor.commit();
                                            dialog.cancel();
                                        } else {
                                            Toast.makeText(ProfileActivity.this, "Le password non sono compatibili", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(ProfileActivity.this, "Vecchia password non corretta", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    alert.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alert.show();
                }
                break;
                case R.id.logout: {
                    vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
                    editor = vPreferences.edit();
                    editor.putBoolean("logged", false);
                    editor.commit();
                    startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                    finish();
                }
                break;
            }
            return false;
        }

    public void changedCeck() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String urlLogin = "http://isfrat.it/wp-content/changePass.php";
        StringRequest sr = new StringRequest(Request.Method.POST, urlLogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.toString().equals("SUCCESS")) {
                    Toast.makeText(ProfileActivity.this, "Changed", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Errore", "" + error.getMessage() + "," + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", vPreferences.getString("username", " "));
                params.put("newPass", pass);
                return params;
            }
        };
        queue.add(sr);
    }

    public void definisciImg() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String urlLogin = "http://isfrat.it/wp-content/addImg.php";
        StringRequest sr = new StringRequest(Request.Method.POST, urlLogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.toString().equals("SUCCESS")) {
                    switch (tipo) {
                        case "deadpool":
                            profilo.setImageResource(R.drawable.utente);
                            break;
                        case "camera":
                            profilo.setImageResource(R.drawable.ic_menu_camera);
                            break;
                        case "android":
                            profilo.setImageResource(android.R.drawable.sym_def_app_icon);
                            break;
                        default:
                            break;
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Errore", "" + error.getMessage() + "," + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", vPreferences.getString("username", " "));
                params.put("profImage", tipo);
                return params;
            }

        };
        queue.add(sr);
    }

    public void setImage1(View v) {
        tipo = "deadpool";
        definisciImg();
        vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
        editor = vPreferences.edit();
        editor.putString("imgProfilo", "deadpool");
        editor.commit();
        img1.setBackgroundColor(Color.parseColor("#ff4646"));
        img2.setBackgroundColor(Color.parseColor("#ffffff"));
        img3.setBackgroundColor(Color.parseColor("#ffffff"));
    }

    public void setImage2(View v) {
        tipo = "camera";
        definisciImg();
        vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
        editor = vPreferences.edit();
        editor.putString("imgProfilo", "camera");
        editor.commit();
        img2.setBackgroundColor(Color.parseColor("#ff4646"));
        img1.setBackgroundColor(Color.parseColor("#ffffff"));
        img3.setBackgroundColor(Color.parseColor("#ffffff"));
    }

    public void setImage3(View v) {
        tipo = "android";
        definisciImg();
        vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
        editor = vPreferences.edit();
        editor.putString("imgProfilo", "android");
        editor.commit();
        img3.setBackgroundColor(Color.parseColor("#ff4646"));
        img1.setBackgroundColor(Color.parseColor("#ffffff"));
        img2.setBackgroundColor(Color.parseColor("#ffffff"));

    }
}
