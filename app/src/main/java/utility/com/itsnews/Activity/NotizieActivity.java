package utility.com.itsnews.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import utility.com.itsnews.Entity.Articolo;
import utility.com.itsnews.Util.ArticoloAdapter;
import utility.com.itsnews.R;
import utility.com.itsnews.Util.ScaricaNotizieSingleton;

public class NotizieActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView listView;
    private ArticoloAdapter adapter;
    private ArrayList<Articolo> listaHome = ScaricaNotizieSingleton.lista;
    private ArrayList<Articolo> listaHomeBackup = listaHome;
    private ArrayList<Articolo> listaSearch;
    private SwipeRefreshLayout pullToRefresh;
    private LinearLayout linearLoading;
    private TextView toolbar_title;
    private ListView listmenu;
    private boolean flag_loading;
    private TextView txtUser;
    private SharedPreferences vPreferences;
    ImageView imgprofilo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notizie);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        listView = (ListView) findViewById(R.id.list);
        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        linearLoading = (LinearLayout) findViewById(R.id.loading);

        View inflater = LayoutInflater.from(this).inflate(R.layout.nav_header_notizie, null);
        txtUser = (TextView) inflater.findViewById(R.id.txtUser);
        imgprofilo = (ImageView) inflater.findViewById(R.id.profile_image);
        vPreferences = inflater.getContext().getSharedPreferences("Preferences", 0);

        txtUser.setText(vPreferences.getString("username", ""));
        switch (vPreferences.getString("imgProfilo", "")) {
            case "deadpool":imgprofilo.setImageResource(R.drawable.utente);break;
            case "android":imgprofilo.setImageResource(android.R.drawable.sym_def_app_icon);break;
            case "camera":imgprofilo.setImageResource(R.drawable.ic_menu_camera);break;
            default:imgprofilo.setImageResource(R.drawable.com_facebook_profile_picture_blank_square);break;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                linearLoading.setVisibility(View.VISIBLE);
                pullToRefresh.setVisibility(View.INVISIBLE);
                printResult(listaHome);
                linearLoading.setVisibility(View.INVISIBLE);
                pullToRefresh.setVisibility(View.VISIBLE);
            }
        }, 4000);

        //listmenu = (ListView) findViewById(R.id.listmenu);

        int id_categoria;
        final Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id_categoria = bundle.getInt("categoria");
            switch (id_categoria) {
                case -1:
                    toolbar_title.setText("ITS News");
                    break;
                case 2:
                    toolbar_title.setText("Cronaca");
                    break;
                case 4:
                    toolbar_title.setText("Attualità");
                    break;
                case 3:
                    toolbar_title.setText("Politica");
                    break;
                case 5:
                    toolbar_title.setText("Tecnologia");
                    break;
                case 6:
                    toolbar_title.setText("Sport");
                    break;
            }

            ScaricaNotizieSingleton singleton = ScaricaNotizieSingleton.getInstance();
            singleton.getNotizie(this, id_categoria, 1, "");
            listaHome = ScaricaNotizieSingleton.lista;
        }


        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // TODO Auto-generated method stub
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ScaricaNotizieSingleton singleton = ScaricaNotizieSingleton.getInstance();
                        singleton.getNotizie(NotizieActivity.this, -1, 1, "");
                        listaHome = ScaricaNotizieSingleton.lista;
                        setLista(-1);
                        pullToRefresh.setRefreshing(false);
                    }
                }, 8000);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(NotizieActivity.this, NotiziaActivity.class);
                Bundle b = new Bundle();
                b.putInt("cod", 0);
                b.putInt("id", listaHome.get(position).getId());
                b.putString("titolo", String.valueOf(listaHome.get(position).getTitolo()));
                b.putString("testo", String.valueOf(listaHome.get(position).getTesto()));
                b.putString("immagine", String.valueOf(listaHome.get(position).getImmagine()));
                b.putString("data", String.valueOf(listaHome.get(position).getData()));
                b.putString("url", String.valueOf(listaHome.get(position).getUrl()));
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int index = 1;
            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    index++;
                    if(!flag_loading)
                    {
                        flag_loading = true;
                        loadOtherNews(index);
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notizie, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                printResult(listaHome);
                return false;
            }
        });

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                return true;
            }

            public boolean onQueryTextSubmit(final String query) {

                if (query.contains(" "))
                    query.replace(" ", "%20");

                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                ScaricaNotizieSingleton singleton = ScaricaNotizieSingleton.getInstance();
                singleton.getNotizie(NotizieActivity.this, -1, 1, query);
                // TODO Auto-generated method stub
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listaSearch = ScaricaNotizieSingleton.lista;
                        printResult(listaSearch);
                    }
                }, 4000);
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            setLista(-1);
        } else if (id == R.id.nav_cronaca) {
            setLista(2);
        } else if (id == R.id.nav_attualita) {
            setLista(4);
        } else if (id == R.id.nav_politica) {
            setLista(3);
        } else if (id == R.id.nav_tecnologia) {
            setLista(5);
        } else if (id == R.id.nav_sport) {
            setLista(6);
        } else if (id == R.id.nav_utente) {
            startActivity(new Intent(NotizieActivity.this, ProfileActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setLista(int id_categoria) {
        Intent intent = new Intent(NotizieActivity.this, NotizieActivity.class);
        Bundle b = new Bundle();
        b.putInt("cod", 1);
        b.putInt("categoria", id_categoria);
        intent.putExtras(b);
        startActivity(intent);
        finish();
    }

    public void printResult(ArrayList<Articolo> lista) {
        adapter = new ArticoloAdapter(NotizieActivity.this, lista);
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void loadOtherNews(final int index) {
        flag_loading = false;

        //listaHome = ScaricaNotizieSingleton.lista;
        ScaricaNotizieSingleton singleton = ScaricaNotizieSingleton.getInstance();
        singleton.getNotizie(NotizieActivity.this, -1, index, "");

        new Handler().postDelayed(new Runnable() {
            boolean download = true;
            @Override
            public void run() {
                if(ScaricaNotizieSingleton.lista.size() > 0 && download) {
                    listaHome.addAll(ScaricaNotizieSingleton.lista);
                    printResult(listaHome);
                    listView.setSelection(listView.getCount() - 10);
                }
                else download = false;
            }
        }, 4000);
    }
}
