package utility.com.itsnews.Activity;

import android.app.DialogFragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import utility.com.itsnews.Util.DatePickerFragment;
import utility.com.itsnews.R;

public class RegistratiActivity extends AppCompatActivity {

    boolean reg;
    private TextView txtDate;
    private String nome, cognome, mail, password, correctPass, data, user;
    private EditText txtEmail, txtRetryPass, txtPass, txtNome, txtCognome, txtUser;
    final Calendar c = Calendar.getInstance();
    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH);
    int day = c.get(Calendar.DAY_OF_MONTH);
    private Intent i;
    private View window;
    private SharedPreferences vPreferences;//preferences to save locally
    private SharedPreferences.Editor editor; //editor

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrati);
        window = findViewById(R.id.activity_reg);

        txtDate = (TextView) findViewById(R.id.txtDate);
        txtUser = (EditText) findViewById(R.id.edtUsername);
        final Button btnRegistrati = (Button) findViewById(R.id.btnRegistrati);
        txtNome = (EditText) findViewById(R.id.edtNome);
        txtCognome = (EditText) findViewById(R.id.edtCognome);
        txtPass = (EditText) findViewById(R.id.edtPass);
        txtRetryPass = (EditText) findViewById(R.id.edtRipetiPass);
        txtEmail = (EditText) findViewById(R.id.edtMail);
        i = new Intent(this, NotizieActivity.class);

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "Date Picker");
            }
        });

        txtRetryPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!txtRetryPass.getText().toString().equals("")) {
                    if (!txtPass.getText().toString().equals(txtRetryPass.getText().toString()))
                        txtRetryPass.setBackgroundColor(Color.parseColor("#ff4646"));
                    else {
                        txtRetryPass.setBackgroundColor(Color.TRANSPARENT);
                        btnRegistrati.setEnabled(true);
                    }
                } else
                    txtRetryPass.setBackgroundColor(Color.TRANSPARENT);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnRegistrati.setEnabled(true);

        btnRegistrati.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                user = txtUser.getText().toString();
                cognome = txtCognome.getText().toString();
                data = txtDate.getText().toString();
                nome = txtNome.getText().toString();
                mail = txtEmail.getText().toString();
                password = txtPass.getText().toString();
                correctPass = txtRetryPass.getText().toString();
                if (cognome.equals("") || nome.equals("") || mail.equals("")) {
                    Toast.makeText(RegistratiActivity.this, "Alcuni campi non sono stati riempiti", Toast.LENGTH_SHORT);
                } else {

                    SharedPreferences vPreferences = window.getContext().getSharedPreferences("Preferences", 0);
                    editor = vPreferences.edit();
                    editor.putString("username", user);
                    editor.putString("email", mail);
                    editor.putString("DataNascita", data);
                    editor.putString("nome", nome);
                    editor.putString("cognome", cognome);
                    editor.putString("password",password);
                    editor.putBoolean("logged", true);
                    editor.commit();
                    registerCheck();


                }

            }
        });
    }

    public boolean controlloPassword(String p, String cp) {

        if (p.equals(cp)) {
            return true;
        } else {
            return false;
        }
    }

    public void registerCheck() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String urlLogin = "http://isfrat.it/wp-content/register.php";
        StringRequest sr = new StringRequest(Request.Method.POST, urlLogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.toString().equals("SUCCESS")) {
                    startActivity(i);
                    finish();
                    Toast.makeText(RegistratiActivity.this, "Registed", Toast.LENGTH_SHORT).show();
                } else {
                    if (response.toString().equals("UN")) {
                        Toast.makeText(RegistratiActivity.this, "Utente già registrato", Toast.LENGTH_SHORT).show();
                    }
                    if (response.toString().equals("EMAIL")) {
                        Toast.makeText(RegistratiActivity.this, "Email già usata", Toast.LENGTH_SHORT).show();
                    }
                    if (response.toString().equals("FAIL")) {
                        reg = true;
                        Toast.makeText(RegistratiActivity.this, "Errore", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Errore", "" + error.getMessage() + "," + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_email", txtEmail.getText().toString());
                params.put("user_password", txtPass.getText().toString());
                params.put("user_un", txtUser.getText().toString());
                params.put("user_name", txtNome.getText().toString());
                params.put("user_surname", txtCognome.getText().toString());
                params.put("birth_date", txtDate.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("abc", "value");
                return headers;
            }
        };
        queue.add(sr);
    }
}
