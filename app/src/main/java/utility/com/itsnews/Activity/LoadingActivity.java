package utility.com.itsnews.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import utility.com.itsnews.Entity.Articolo;
import utility.com.itsnews.R;
import utility.com.itsnews.Util.ScaricaNotizieSingleton;

public class LoadingActivity extends AppCompatActivity {

    public static ArrayList<Articolo> lista = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        getSupportActionBar().hide();
        SharedPreferences vPreferences = findViewById(R.id.startscreen).getContext().getSharedPreferences("Preferences", 0);
        if(vPreferences.getBoolean("logged", false)) {
            // Utente loggato
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ScaricaNotizieSingleton singleton = ScaricaNotizieSingleton.getInstance();
                    singleton.getNotizie(LoadingActivity.this, -1, 1, "");
                    lista = ScaricaNotizieSingleton.lista;
                    Intent i = new Intent(LoadingActivity.this, NotizieActivity.class);
                    startActivity(i);
                    finish();
                }
            }, 7000);
        }
        else {
            // Utente non loggato
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ScaricaNotizieSingleton singleton = ScaricaNotizieSingleton.getInstance();
                    singleton.getNotizie(LoadingActivity.this, -1, 1, "");
                    lista = ScaricaNotizieSingleton.lista;
                    Intent i = new Intent(LoadingActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }, 7000);
        }
    }
}