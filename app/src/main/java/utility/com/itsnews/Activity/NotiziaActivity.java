package utility.com.itsnews.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

import utility.com.itsnews.Entity.Articolo;
import utility.com.itsnews.R;
import utility.com.itsnews.Util.ArticoliCategoriaAdapter;
import utility.com.itsnews.Util.ScaricaNotizieSingleton;

public class NotiziaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ImageButton btnFacebook;
    private ImageButton btnGooglepiu;
    private ImageButton btnWhatsapp;
    private LinearLayout llay;
    private ListView listView;
    private ArrayList<Articolo> lista = new ArrayList<>();
    private TextView txtUser;
    private View window;
    private SharedPreferences vPreferences;
    ImageView imgprofilo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notizia);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        btnFacebook = (ImageButton) findViewById(R.id.btnFacebook);
        btnGooglepiu = (ImageButton) findViewById(R.id.btnGooglepiu);
        btnWhatsapp = (ImageButton) findViewById(R.id.btnWhatsapp);
        listView = (ListView) findViewById(R.id.listaCategoria);
        llay = (LinearLayout) findViewById(R.id.llay);

        View inflater = LayoutInflater.from(this).inflate(R.layout.nav_header_notizie, null);
        txtUser = (TextView) inflater.findViewById(R.id.txtUser);
        imgprofilo = (ImageView) inflater.findViewById(R.id.profile_image);
        vPreferences = inflater.getContext().getSharedPreferences("Preferences", 0);

        txtUser.setText(vPreferences.getString("username", ""));
        switch (vPreferences.getString("imgProfilo", "")) {
            case "deadpool":
                imgprofilo.setImageResource(R.drawable.utente);
                break;
            case "android":
                imgprofilo.setImageResource(android.R.drawable.sym_def_app_icon);
                break;
            case "camera":
                imgprofilo.setImageResource(R.drawable.ic_menu_camera);
                break;
            default:
                imgprofilo.setImageResource(R.drawable.com_facebook_profile_picture_blank_square);
                break;
        }

        final Articolo art = new Articolo();
        int id_categoria = -1;
        final Bundle bundle = getIntent().getExtras();
        int cod = bundle.getInt("cod");
        if (cod == 0) {
            art.setId(bundle.getInt("id"));
            art.setTitolo(bundle.getString("titolo"));
            art.setTesto(bundle.getString("testo"));
            art.setImmagine(bundle.getString("immagine"));
            art.setData(bundle.getString("data"));
            art.setUrl(bundle.getString("url"));
            lista.add(art);
            settaAdapter(lista);
        } else {
            llay.setVisibility(LinearLayout.GONE);
            id_categoria = bundle.getInt("categoria");
            ScaricaNotizieSingleton singleton = ScaricaNotizieSingleton.getInstance();
            singleton.getNotizie(this, id_categoria, 1, "");
            lista = LoadingActivity.lista;
        }

        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnGooglepiu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = ShareCompat.IntentBuilder.from(NotiziaActivity.this)
                        .setType("text/plain")
                        .setText(art.getUrl())
                        .getIntent()
                        .setPackage("com.google.android.apps.plus");

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),"Google+ non installato.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, art.getUrl());
            try {
                startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(),"Whatsapp non installato.", Toast.LENGTH_SHORT).show();
            }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        String id = outState.getString("id");
        outState.putString("id", id);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notizia, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            lista.clear();
            setLista(-1, 1);
        } else if (id == R.id.nav_cronaca) {
            setLista(2, 0);
        } else if (id == R.id.nav_attualita) {
            setLista(4, 0);
        } else if (id == R.id.nav_politica) {
            setLista(3, 0);
        } else if (id == R.id.nav_tecnologia) {
            setLista(5, 0);
        } else if (id == R.id.nav_sport) {
            setLista(6, 0);
        } else if (id == R.id.nav_utente) {
            startActivity(new Intent(NotiziaActivity.this,ProfileActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void settaAdapter(ArrayList<Articolo> lista) {
        ArticoliCategoriaAdapter adapter = new ArticoliCategoriaAdapter(NotiziaActivity.this, lista);
        listView.setAdapter(adapter);
    }

    public void setLista(int id_categoria, int id_activity) {
        //id_activity 0=NotiziaActivity 1=NotizieActivity
        Intent intent = null;
        switch (id_activity) {
            case 0: intent = new Intent(NotiziaActivity.this, NotiziaActivity.class);break;
            case 1: intent = new Intent(NotiziaActivity.this, NotizieActivity.class);break;
        }
        Bundle b = new Bundle();
        b.putInt("cod", 1);
        b.putInt("categoria", id_categoria);
        intent.putExtras(b);
        startActivity(intent);
        finish();
    }
}
